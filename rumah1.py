from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-100.0, 100.0, -100.0, 100.0)
    glPointSize(10)


def plotpoints():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_POINTS)
    glVertex2f(0.0, 0.0)
    glEnd()
    glFlush()


def plotlines():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINES)

    # Garis atap rumah bagian bawah

    glVertex2f(-50.0, 20.0)
    glVertex2f(50.0, 20.0)

    # Garis atap rumah bagian atas

    glVertex2f(-40.0, 40.0)
    glVertex2f(40.0, 40.0)

    # Garis atap rumah bagian kiri

    glVertex2f(-50.0, 20.0)
    glVertex2f(-40.0, 40.0)

    # Garis atap rumah bagian kanan

    glVertex2f(50.0, 20.0)
    glVertex2f(40.0, 40.0)

    # Cerobong asap bagian kiri

    glVertex2f(-30, 40)
    glVertex2f(-30, 50)

    glVertex2f(-20, 40)
    glVertex2f(-20, 50)

    glVertex2f(-30, 50)
    glVertex2f(-35, 50)

    glVertex2f(-35, 50)
    glVertex2f(-35, 53)

    glVertex2f(-35, 53)
    glVertex2f(-15, 53)

    glVertex2f(-15, 53)
    glVertex2f(-15, 50)

    glVertex2f(-15, 50)
    glVertex2f(-20, 50)

    # Cerobong asap bagian kanan

    glVertex2f(30, 40)
    glVertex2f(30, 50)

    glVertex2f(20, 40)
    glVertex2f(20, 50)

    glVertex2f(30, 50)
    glVertex2f(35, 50)

    glVertex2f(35, 50)
    glVertex2f(35, 53)

    glVertex2f(35, 53)
    glVertex2f(15, 53)

    glVertex2f(15, 53)
    glVertex2f(15, 50)

    glVertex2f(15, 50)
    glVertex2f(20, 50)

    # Simbol di bagian atap

    glVertex2f(-3, 25)
    glVertex2f(3, 25)

    glVertex2f(-3, 25)
    glVertex2f(-3, 33)

    glVertex2f(-3, 33)
    glVertex2f(-5, 33)

    glVertex2f(-5, 33)
    glVertex2f(-5, 35)

    glVertex2f(-5, 35)
    glVertex2f(5, 35)

    glVertex2f(5, 35)
    glVertex2f(5, 33)

    glVertex2f(5, 33)
    glVertex2f(3, 33)

    glVertex2f(3, 33)
    glVertex2f(3, 25)

    glVertex2f(0, 35)
    glVertex2f(0, 25)

    # Kotak rumah

    glVertex2f(-40, 20)
    glVertex2f(-40, -40)

    glVertex2f(-40, -40)
    glVertex2f(40, -40)

    glVertex2f(40, -40)
    glVertex2f(40, 20)

    # Jendela tengah

    glVertex2f(-10, 15)
    glVertex2f(10, 15)

    glVertex2f(10, 15)
    glVertex2f(10, 0)

    glVertex2f(10, 0)
    glVertex2f(-10, 0)

    glVertex2f(-10, 0)
    glVertex2f(-10, 15)

    glVertex2f(0, 15)
    glVertex2f(0, 0)

    glVertex2f(-10, 7.5)
    glVertex2f(10, 7.5)

    # Jendela kanan

    glVertex2f(15, 15)
    glVertex2f(35, 15)

    glVertex2f(35, 15)
    glVertex2f(35, 0)

    glVertex2f(35, 0)
    glVertex2f(15, 0)

    glVertex2f(15, 0)
    glVertex2f(15, 15)

    glVertex2f(25, 15)
    glVertex2f(25, 0)

    glVertex2f(15, 7.5)
    glVertex2f(35, 7.5)

    # Jendela kiri

    glVertex2f(-15, 15)
    glVertex2f(-35, 15)

    glVertex2f(-35, 15)
    glVertex2f(-35, 0)

    glVertex2f(-35, 0)
    glVertex2f(-15, 0)

    glVertex2f(-15, 0)
    glVertex2f(-15, 15)

    glVertex2f(-25, 15)
    glVertex2f(-25, 0)

    glVertex2f(-15, 7.5)
    glVertex2f(-35, 7.5)

    # jendela bawah bagian kiri

    glVertex2f(-15, -20)
    glVertex2f(-35, -20)

    glVertex2f(-35, -20)
    glVertex2f(-35, -35)

    glVertex2f(-35, -35)
    glVertex2f(-15, -35)

    glVertex2f(-15, -35)
    glVertex2f(-15, -20)

    glVertex2f(-25, -20)
    glVertex2f(-25, -35)

    glVertex2f(-35, -27.5)
    glVertex2f(-15, -27.5)

    # jendela bawah bagian kanan

    glVertex2f(15, -20)
    glVertex2f(35, -20)

    glVertex2f(35, -20)
    glVertex2f(35, -35)

    glVertex2f(35, -35)
    glVertex2f(15, -35)

    glVertex2f(15, -35)
    glVertex2f(15, -20)

    glVertex2f(25, -20)
    glVertex2f(25, -35)

    glVertex2f(35, -27.5)
    glVertex2f(15, -27.5)

    # Garis rumah bagian bawah

    glVertex2f(-40, -40)
    glVertex2f(-50, -40)

    glVertex2f(-50, -40)
    glVertex2f(-50, -45)

    glVertex2f(-50, -45)
    glVertex2f(50, -45)

    glVertex2f(50, -45)
    glVertex2f(50, -40)

    glVertex2f(50, -40)
    glVertex2f(40, -40)

    # Tangga rumah bagian bawah

    glVertex2f(-10, -47)
    glVertex2f(10, -47)

    glVertex2f(-10, -47)
    glVertex2f(-10, -42)

    glVertex2f(-10, -42)
    glVertex2f(10, -42)

    glVertex2f(10, -42)
    glVertex2f(10, -47)

    # Tangga rumah bagian atas

    glVertex2f(-9, -42)
    glVertex2f(-9, -37)

    glVertex2f(-9, -37)
    glVertex2f(9, -37)

    glVertex2f(9, -37)
    glVertex2f(9, -42)

    # Pintu rumah

    glVertex2f(-8, -37)
    glVertex2f(-8, -10)

    glVertex2f(8, -37)
    glVertex2f(8, -10)

    glVertex2f(-8, -10)
    glVertex2f(8, -10)

    glEnd()
    glFlush()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Rumah 1")
    glutDisplayFunc(plotlines)
    init()
    glutMainLoop()


main()
