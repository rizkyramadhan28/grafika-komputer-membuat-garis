from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-100.0, 100.0, -100.0, 100.0)
    glPointSize(10)


def plotlines():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINES)

    # Garis atap rumah bagian atas

    glVertex2f(-40.0, 60.0)
    glVertex2f(40.0, 60.0)

    # Garis atap rumah bagian bawah

    glVertex2f(-50.0, 40.0)
    glVertex2f(50.0, 40.0)

    # Garis atap rumah bagian kiri

    glVertex2f(-50.0, 40.0)
    glVertex2f(-40.0, 60.0)

    # Garis atap rumah bagian kanan

    glVertex2f(50.0, 40.0)
    glVertex2f(40.0, 60.0)

    # Garis segitiga sebelah kiri

    glVertex2f(-30.0, 40.0)
    glVertex2f(0.0, 75.0)

    # Garis segitiga sebelah kanan

    glVertex2f(30.0, 40.0)
    glVertex2f(0.0, 75.0)

    # Kotak rumah

    glVertex2f(-40.0, 40.0)
    glVertex2f(-40.0, 0.0)

    glVertex2f(-40.0, 0.0)
    glVertex2f(40.0, 0.0)

    glVertex2f(40.0, 0.0)
    glVertex2f(40.0, 40.0)

    # Tangga rumah

    glVertex2f(-40.0, 0.0)
    glVertex2f(-50.0, 0.0)

    glVertex2f(-50.0, 0.0)
    glVertex2f(-50.0, -10.0)

    glVertex2f(-50.0, -10.0)
    glVertex2f(50.0, -10.0)

    glVertex2f(50.0, -10.0)
    glVertex2f(50.0, 0.0)

    glVertex2f(50.0, 0.0)
    glVertex2f(40.0, 0.0)

    # Pintu rumah

    glVertex2f(-10.0, 0.0)
    glVertex2f(-10.0, 30.0)

    glVertex2f(10.0, 0.0)
    glVertex2f(10.0, 30.0)

    glVertex2f(-10.0, 30.0)
    glVertex2f(10.0, 30.0)

    glVertex2f(-13.0, 26.0)
    glVertex2f(13.0, 26.0)

    glVertex2f(13.0, 26.0)
    glVertex2f(13.0, 22.0)

    glVertex2f(13.0, 22.0)
    glVertex2f(-13.0, 22.0)

    glVertex2f(-13.0, 22.0)
    glVertex2f(-13.0, 26.0)

    # Jendele kiri

    glVertex2f(-35.0, 30.0)
    glVertex2f(-18.0, 30.0)

    glVertex2f(-35.0, 30.0)
    glVertex2f(-35.0, 10.0)

    glVertex2f(-35.0, 10.0)
    glVertex2f(-18.0, 10.0)

    glVertex2f(-18.0, 10.0)
    glVertex2f(-18.0, 30.0)

    glVertex2f(-30.0, 30.0)
    glVertex2f(-30.0, 10.0)

    glVertex2f(-23.0, 30.0)
    glVertex2f(-23.0, 10.0)

    glVertex2f(-35.0, 20.0)
    glVertex2f(-30.0, 20.0)

    glVertex2f(-23.0, 20.0)
    glVertex2f(-18.0, 20.0)

    # Jendele kiri

    glVertex2f(35.0, 30.0)
    glVertex2f(18.0, 30.0)

    glVertex2f(35.0, 30.0)
    glVertex2f(35.0, 10.0)

    glVertex2f(35.0, 10.0)
    glVertex2f(18.0, 10.0)

    glVertex2f(18.0, 10.0)
    glVertex2f(18.0, 30.0)

    glVertex2f(30.0, 30.0)
    glVertex2f(30.0, 10.0)

    glVertex2f(23.0, 30.0)
    glVertex2f(23.0, 10.0)

    glVertex2f(35.0, 20.0)
    glVertex2f(30.0, 20.0)

    glVertex2f(23.0, 20.0)
    glVertex2f(18.0, 20.0)

    # Tangga paling bawah

    glVertex2f(-15.0, -10.0)
    glVertex2f(-15.0, -6.0)

    glVertex2f(-15.0, -6.0)
    glVertex2f(15.0, -6.0)

    glVertex2f(15.0, -6.0)
    glVertex2f(15.0, -10.0)

    # Tangga tengah

    glVertex2f(-12.0, -6.0)
    glVertex2f(-12.0, -3.0)

    glVertex2f(-12.0, -3.0)
    glVertex2f(12.0, -3.0)

    glVertex2f(12.0, -3.0)
    glVertex2f(12.0, -6.0)

    # Tangga atas

    glVertex2f(-10.0, -3.0)
    glVertex2f(-10.0, 0.0)

    glVertex2f(10.0, 0.0)
    glVertex2f(10.0, -3.0)

    # Simbol

    glVertex2f(-6.0, 57.0)
    glVertex2f(6.0, 57.0)

    glVertex2f(6.0, 57.0)
    glVertex2f(6.0, 43.0)

    glVertex2f(6.0, 43.0)
    glVertex2f(-6.0, 43.0)

    glVertex2f(-6.0, 43.0)
    glVertex2f(-6.0, 57.0)

    glVertex2f(-3.0, 53.0)
    glVertex2f(-3.0, 43.0)

    glVertex2f(3.0, 53.0)
    glVertex2f(3.0, 43.0)

    glVertex2f(-10.0, 55.0)
    glVertex2f(10.0, 55.0)

    glVertex2f(-10.0, 55.0)
    glVertex2f(-10.0, 53.0)

    glVertex2f(-10.0, 53.0)
    glVertex2f(10.0, 53.0)

    glVertex2f(10.0, 53.0)
    glVertex2f(10.0, 55.0)

    glVertex2f(-6.0, 48.0)
    glVertex2f(-3.0, 48.0)

    glVertex2f(6.0, 48.0)
    glVertex2f(3.0, 48.0)

    glEnd()
    glFlush()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Rumah 2")
    glutDisplayFunc(plotlines)
    init()
    glutMainLoop()


main()
