from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-100.0, 100.0, -100.0, 100.0)
    glPointSize(10)


def plotlines():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINES)

    # Kotakan rumah

    glVertex2f(-40.0, 20.0)
    glVertex2f(-40.0, -20.0)

    glVertex2f(-40.0, -20.0)
    glVertex2f(40.0, -20.0)

    glVertex2f(40.0, -20.0)
    glVertex2f(40.0, 20.0)

    # Atap rumah

    glVertex2f(-40.0, 20.0)
    glVertex2f(-50.0, 20.0)

    glVertex2f(-50.0, 20.0)
    glVertex2f(0.0, 60.0)

    glVertex2f(0.0, 60.0)
    glVertex2f(50.0, 20.0)

    glVertex2f(50.0, 20.0)
    glVertex2f(40.0, 20.0)

    glVertex2f(-40.0, 20.0)
    glVertex2f(0.0, 50.0)

    glVertex2f(0.0, 50.0)
    glVertex2f(40.0, 20.0)

    # Cerobong asap

    glVertex2f(-25.0, 40.0)
    glVertex2f(-25.0, 50.0)

    glVertex2f(-40.0, 28.0)
    glVertex2f(-40.0, 50.0)

    glVertex2f(-40.0, 50.0)
    glVertex2f(-45.0, 50.0)

    glVertex2f(-45.0, 50.0)
    glVertex2f(-45.0, 53.0)

    glVertex2f(-45.0, 53.0)
    glVertex2f(-20.0, 53.0)

    glVertex2f(-20.0, 53.0)
    glVertex2f(-20.0, 50.0)

    glVertex2f(-20.0, 50.0)
    glVertex2f(-40.0, 50.0)

    # Tangga

    glVertex2f(-40.0, -20.0)
    glVertex2f(-50.0, -20.0)

    glVertex2f(-50.0, -20.0)
    glVertex2f(-50.0, -30.0)

    glVertex2f(-50.0, -30.0)
    glVertex2f(50.0, -30.0)

    glVertex2f(50.0, -30.0)
    glVertex2f(50.0, -20.0)

    glVertex2f(50.0, -20.0)
    glVertex2f(40.0, -20.0)

    # Tangga bawah

    glVertex2f(-15.0, -30.0)
    glVertex2f(-15.0, -26.0)

    glVertex2f(-15.0, -26.0)
    glVertex2f(15.0, -26.0)

    glVertex2f(15.0, -26.0)
    glVertex2f(15.0, -30.0)

    # Tangga tengah

    glVertex2f(-12.0, -26.0)
    glVertex2f(-12.0, -23.0)

    glVertex2f(-12.0, -23.0)
    glVertex2f(12.0, -23.0)

    glVertex2f(12.0, -23.0)
    glVertex2f(12.0, -26.0)

    # Tangga atas

    glVertex2f(-10.0, -23.0)
    glVertex2f(-10.0, -20.0)

    glVertex2f(10.0, -23.0)
    glVertex2f(10.0, -20.0)

    # Pintu

    glVertex2f(-10.0, -20.0)
    glVertex2f(-10.0, 10.0)

    glVertex2f(-10.0, 10.0)
    glVertex2f(10.0, 10.0)

    glVertex2f(10.0, 10.0)
    glVertex2f(10.0, -20.0)

    glVertex2f(-15.0, 6.0)
    glVertex2f(-15.0, 2.0)

    glVertex2f(-15.0, 2.0)
    glVertex2f(15.0, 2.0)

    glVertex2f(15.0, 2.0)
    glVertex2f(15.0, 6.0)

    glVertex2f(15.0, 6.0)
    glVertex2f(-15.0, 6.0)

    glVertex2f(-8.0, 10.0)
    glVertex2f(-8.0, 13.0)

    glVertex2f(-8.0, 13.0)
    glVertex2f(8.0, 13.0)

    glVertex2f(8.0, 13.0)
    glVertex2f(8.0, 10.0)

    # Jendela kiri

    glVertex2f(-37.0, 10.0)
    glVertex2f(-37.0, -10.0)

    glVertex2f(-37.0, -10.0)
    glVertex2f(-18.0, -10.0)

    glVertex2f(-18.0, -10.0)
    glVertex2f(-18.0, 10.0)

    glVertex2f(-18.0, 10.0)
    glVertex2f(-37.0, 10.0)

    glVertex2f(-27.5, 10.0)
    glVertex2f(-27.5, -10.0)

    glVertex2f(-27.5, 2.0)
    glVertex2f(-18, 2.0)

    # Jendela kanan

    glVertex2f(37.0, 10.0)
    glVertex2f(37.0, -10.0)

    glVertex2f(37.0, -10.0)
    glVertex2f(18.0, -10.0)

    glVertex2f(18.0, -10.0)
    glVertex2f(18.0, 10.0)

    glVertex2f(18.0, 10.0)
    glVertex2f(37.0, 10.0)

    glVertex2f(27.5, 10.0)
    glVertex2f(27.5, -10.0)

    glVertex2f(28, 2.0)
    glVertex2f(37, 2.0)

    # Jendela atas

    glVertex2f(-10, 37.0)
    glVertex2f(10, 37.0)

    glVertex2f(10, 37.0)
    glVertex2f(10, 18.0)

    glVertex2f(10, 18.0)
    glVertex2f(-10, 18.0)

    glVertex2f(-10, 18.0)
    glVertex2f(-10, 37.0)

    glVertex2f(0.0, 37.0)
    glVertex2f(0.0, 18.0)

    glVertex2f(0.0, 29.0)
    glVertex2f(10.0, 29.0)

    glEnd()
    glFlush()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Rumah 3")
    glutDisplayFunc(plotlines)
    init()
    glutMainLoop()


main()
